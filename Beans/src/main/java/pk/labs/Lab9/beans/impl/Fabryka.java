/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

import java.beans.PropertyVetoException;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.ConsultationList;
import pk.labs.Lab9.beans.ConsultationListFactory;

/**
 *
 * @author st
 */
public class Fabryka implements ConsultationListFactory {
    
    @Override
    public ConsultationList create() {
        return new ListaKonsultacji();
    }

    @Override
    public ConsultationList create(boolean deserialize) {
        Consultation[] wynik = null;
        ConsultationList cl = new ListaKonsultacji();
        if(deserialize){
        try{
            XMLDecoder decoder = new XMLDecoder(
                    new BufferedInputStream(
                    new FileInputStream("save.xml")));
            
            wynik = (Consultation[])decoder.readObject();
            decoder.close();
        if(wynik != null){
        for(int i=0; i < wynik.length ; i++ )
        {
            try {
                cl.addConsultation(wynik[i]);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(Fabryka.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        }
            }
        catch(FileNotFoundException e) {
            System.out.print(e.getMessage());
        }                
      
        }
        else create();
        return cl;
        
    }

    @Override
    public void save(ConsultationList consultationList) {
        
        try{
            XMLEncoder encoder = new XMLEncoder(
                    new BufferedOutputStream(
                    new FileOutputStream("save.xml")));

            encoder.writeObject(consultationList.getConsultation());
            encoder.close();
        }
        catch(FileNotFoundException e){
            System.out.print(e.getMessage());
        }
        }
             
}
