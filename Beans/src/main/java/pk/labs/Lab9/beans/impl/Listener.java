/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.util.Date;
import pk.labs.Lab9.beans.ConsultationList;
import pk.labs.Lab9.beans.Term;

/**
 *
 * @author Adam
 */
public class Listener implements VetoableChangeListener {

    private ConsultationList lista;
    
    public Listener(ConsultationList lista)
    {
        this.lista = lista;
    }
    
    @Override
    public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
        
        Term stary = (Term)evt.getOldValue();
        Term nowy = (Term)evt.getNewValue();

        for(int i=0; i < this.lista.getSize();i++)
        {           
            if(nowy.getEnd().after(this.lista.getConsultation(i).getBeginDate()) 
                    && nowy.getBegin().before(this.lista.getConsultation(i).getEndDate()) 
                    && !stary.getBegin().equals(this.lista.getConsultation(i).getBeginDate()))
            {
                throw new PropertyVetoException("Nie mozna dodac!", null);
            }
        }
    }
    
}
