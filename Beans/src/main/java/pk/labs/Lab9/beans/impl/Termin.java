/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

import java.util.Date;
import java.util.HashSet;
import javax.xml.datatype.Duration;
import pk.labs.Lab9.beans.Term;

/**
 *
 * @author st
 */
public class Termin implements Term {

    private Date begin;
    
    private int duration;
    
    public Termin(){}
    
    @Override
    public Date getBegin() {
        return this.begin;
    }

    @Override
    public void setBegin(Date begin) {
        this.begin = begin;              
    }

    @Override
    public int getDuration() {
        return  this.duration;
    }

    @Override
    public void setDuration(int duration) {
        if(duration > 0)       
            this.duration = duration; 
        else 
            this.duration = 30;       
    }

    @Override
    public Date getEnd() {
        return new Date(this.begin.getTime() + this.duration * 60000);
    }
    
}
