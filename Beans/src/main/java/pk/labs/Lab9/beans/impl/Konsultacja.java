/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import java.util.Date;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.Term;

/**
 *
 * @author st
 */
public class Konsultacja implements Consultation {

    private final VetoableChangeSupport vcs = new VetoableChangeSupport(this);
    private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    
    private String student;
    private Term term;
    
    public Konsultacja(){}
    
    @Override
    public String getStudent() {
        return this.student;
    }

    @Override
    public void setStudent(String student) {
        this.student = student;
    }

    @Override
    public Date getBeginDate() {
        return this.term.getBegin();
    }

    @Override
    public Date getEndDate() {
        return this.term.getEnd();
    }

    @Override
    public void setTerm(Term term) throws PropertyVetoException {
        Term oldTerm = this.term;
        vcs.fireVetoableChange("term", oldTerm, term);
        this.term = term;
        pcs.firePropertyChange("term", oldTerm, term);
        
    }
    
    public Term getTerm()
    {
        return this.term;
    }

    @Override
    public void prolong(int minutes) throws PropertyVetoException {    
        if(minutes <= 0) minutes=0;  
        Term oldTerm = this.term;
        Term newTerm = new Termin();
        newTerm.setBegin(oldTerm.getBegin());
        newTerm.setDuration(minutes + oldTerm.getDuration());
        this.vcs.fireVetoableChange("term", oldTerm, newTerm);
        this.term = newTerm;
        this.pcs.firePropertyChange("term", oldTerm, newTerm);
    }
    
    public void dodajVeto(Listener listener)
    {
        this.vcs.addVetoableChangeListener(listener);
    }
    
}
